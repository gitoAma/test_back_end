package org.test.controller;

import lombok.extern.java.Log;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.test.model.LogNLE;
import org.test.repository.LogNLERepository;

import java.util.Optional;

@RestController
public class MainController {

    @Autowired
    private LogNLERepository logNLERepository;

    @PostMapping("/post")
    public void insertData(@RequestBody LogNLE logNLE) {
        logNLERepository.save(logNLE);

    }

    @GetMapping("/get/{id]")
    public LogNLE getData(@PathVariable String id) {
        Optional<LogNLE> byId = logNLERepository.findById(id);
        return byId.orElse(null);

    }

    @PutMapping("/put")
    public void putData(@RequestBody LogNLE logNLE) {
        logNLERepository.save(logNLE);

    }

    @DeleteMapping("/del/{id}")
    public void deleteData(@PathVariable String id) {
        logNLERepository.deleteById(id);

    }
}
