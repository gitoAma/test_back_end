package org.test.repository;

import org.springframework.data.repository.CrudRepository;
import org.test.model.LogNLE;

public interface LogNLERepository extends CrudRepository<LogNLE,String> {
}
