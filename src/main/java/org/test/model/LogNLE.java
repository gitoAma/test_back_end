package org.test.model;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Data
@NoArgsConstructor
@Entity
@Table(name = "ama_muzni_m")
public class LogNLE {
    @Id
    private String idRequestBooking;
    private String  idPlatformCharacter;
    private String namaPlatform;
    private String docType;
    private String termOfPayment;

}
